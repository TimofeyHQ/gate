#ifndef TEXTCONSOLEWIDGET_H
#define TEXTCONSOLEWIDGET_H

#include <QPlainTextEdit>
#include <QTextStream>
#include <iostream>
#include "screen.h"

class TextConsoleWidget : public QPlainTextEdit
{
    Q_OBJECT
private:
    QTextStream* kbStream;
public:
    explicit TextConsoleWidget(QWidget *parent = 0, Qt::WindowFlags f = 0);

    void setStream(QTextStream *stream);

    void wheelEvent(QWheelEvent *e);
    void keyPressEvent(QKeyEvent *e);

    void update(const Screen& s);
};

#endif // TEXTCONSOLEWIDGET_H
