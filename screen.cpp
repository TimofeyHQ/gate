#include "screen.h"

void Screen::cls(unsigned short begin, unsigned short end, unsigned char color)
{
    unsigned char beginV = ((begin & 0xff00) >> 8);
    unsigned char beginH = (begin & 0x00ff);

    unsigned char endV = ((end & 0xff00) >> 8);
    unsigned char endH = (end & 0x00ff);

    if(sType == text)
    {
        for(int i = beginV; i <= endV; ++i)
        {
            for(int j = beginH; j <= endH; ++j)
            {
                charField[i][j] = ' ';
            }
        }

        cursor = std::make_pair<int, int>(endV, endH);
    }
    else
    {
        QRgb c = colors[(color & 0x0f)];

        for(int i = beginV; i <= endV; ++i)
        {
            for(int j = beginH; j <= endH; ++j)
            {
                image.setPixel(j, i, c);
            }
        }
    }
}

void Screen::reset()
{
    setScreenType(text);

    cursor = std::make_pair<int, int>(0, 0);
}

Screen::Screen() : sType(text),
    screenSize({{25, 80}, {200, 320}, {350, 640}}),
    charField(screenSize[text].first, std::vector<QChar>(screenSize[text].second, ' ')),
    cyrillic(QTextCodec::codecForName("CP-866")),
    image(screenSize[graphics16].second, screenSize[graphics16].first, QImage::Format_RGB32),
    cursor(0, 0)
{
    colors[0x0] = 0x000000; //black
    colors[0x1] = 0x000080; //blue
    colors[0x2] = 0x008000; //green
    colors[0x3] = 0x008080; //cyan
    colors[0x4] = 0x800000; //red
    colors[0x5] = 0x808000; //magenta
    colors[0x6] = 0x800080; //brown
    colors[0x7] = 0xc0c0c0; //white
    colors[0x8] = 0x808080; //gray
    colors[0x9] = 0x0000FF; //bright blue
    colors[0xA] = 0x00FF00; //bright green
    colors[0xB] = 0x00FFFF; //bright cyan
    colors[0xC] = 0xFF0000; //bright red
    colors[0xD] = 0xFF00FF; //bright magenta
    colors[0xE] = 0xFFFF00; //bright yellow
    colors[0xF] = 0xFFFFFF; //bright white

    image.fill(0x0);
}

Screen::~Screen() {}

const std::pair<int, int> &Screen::getCursorPos() const
{
    return cursor;
}

void Screen::setCursorPos(const std::pair<int, int> pos)
{
    cursor = pos;
}

void Screen::print(const char *c)
{
    char ch = *c;

    if(ch == 0x0D) newLine();
    else if(ch == 0x0A) cursor.second = 0;
    else
    {
        QString temp = cyrillic->toUnicode(c, 1);

        charField[cursor.first][cursor.second] = temp.at(0);

        if(cursor.second < (screenSize[sType].second - 1))
        {
            ++cursor.second;
        }
        else
        {
            cursor.second = 0;
            newLine();
        }
    }
}

void Screen::drawPoint(const unsigned int &v, const unsigned int &h, const char &color)
{
    image.setPixel(h, v, colors[(color & 0x0f)]);
}

void Screen::setPalette(const char &color)
{
    if(sType != text)
    {
        std::cout << std::hex << ((color & 0xf0) >> 4) << std::endl;
        image.fill(colors[((color & 0xf0) >> 4)]);
    }
}

void Screen::newLine()
{
    if(cursor.first < (screenSize[sType].first - 1))
    {
        ++cursor.first;
    }
    else
    {
        for(auto i = 1; i < screenSize[sType].first; ++i)
        {
            charField[i - 1] = charField[i];
        }
        for(int i = 1; i < screenSize[sType].second; ++i)
        {
            charField[screenSize[sType].first - 1][i] = ' ';
        }
    }
}

QImage Screen::getImage() const
{
    return image;
}

void Screen::setScreenType(Screen::ScreenType t)
{
    sType = t;

    for(auto i = charField.begin(); i != charField.end(); ++i)
    {
        for(auto j = i->begin(); j != i->end(); ++j)
        {
            *j = ' ';
        }
    }

    if(sType != text) image = QImage(screenSize[sType].second, screenSize[sType].first, QImage::Format_RGB32);
    image.fill(0x0);
}

const Screen::ScreenType &Screen::getScreenType() const
{
    return sType;
}

const int &Screen::getScreenHeight() const
{
    return screenSize[sType].first;
}

const int &Screen::getScreenWidth() const
{
    return screenSize[sType].second;
}

const std::vector<std::vector<QChar> > &Screen::getCharField() const
{
    return charField;
}
