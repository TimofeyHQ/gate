#ifndef LINEDELEGATE_H
#define LINEDELEGATE_H

#include <QPlainTextEdit>
#include <QKeyEvent>

class LineDelegate : public QPlainTextEdit {
    Q_OBJECT
public:
  LineDelegate(QWidget*parent=0);
  void setNextCurPos(int n);
private:
  void keyPressEvent(QKeyEvent* e);
signals:
  void changeRow(int r);
  void editingFinished();
};
#endif // LINEDELEGATE_H
