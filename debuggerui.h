#ifndef DEBUGGERUI_H
#define DEBUGGERUI_H

#include <QWidget>
#include <QListWidget>
#include <QDebug>
#include <QtConcurrent/QtConcurrentRun>
#include <QFutureWatcher>
#include <QTextStream>
#include <QTimer>
#include <QMessageBox>
#include "screenwidget.h"
#include "debugger.h"

namespace Ui {
class DebuggerUI;
}

class DebuggerUI : public QWidget
{
    Q_OBJECT

public:
    explicit DebuggerUI(QWidget *parent = 0, Qt::WindowFlags f = 0);
    ~DebuggerUI();

    void init(const QVector<QPair<int, QString> > &lines, QByteArray * const memory, QHash<QString, Register> * const segRegisters);

private slots:
    void error(int line);

    void handleFinished();
    void updateScreen(bool forceShow = false);
    void updateFields();

    void on_runbtn_clicked();
    void on_trmbtn_clicked();
    void on_scrbtn_clicked();

    void on_dbgbtn_clicked();

    void on_nBPbtn_clicked();

    void timedUpdate();

private:
    bool updateFinished = true;

    Ui::DebuggerUI *ui;

    QTextStream* kbStream;
    QTextStream* errStream;

    Debugger* debug;
    ScreenWidget* screenWidget;

    QTimer *updateTimer;

    void closeEvent(QCloseEvent *);

    QFutureWatcher<bool> execWatcher;

    void run(bool debug = false, QVector<bool> breakPoints = QVector<bool>());

    QVector<QPair<int, QString>> src;

    QString errBuffer;
    QString kbBuffer;
};

#endif // DEBUGGERUI_H
