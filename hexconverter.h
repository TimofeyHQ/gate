#ifndef HEXCONVERTER_H
#define HEXCONVERTER_H

#include <QWidget>
#include <QDebug>
#include <QString>
#include <QValidator>
#include <QLineEdit>

namespace Ui {
class HexConverter;
}

class HexConverter : public QWidget
{
    Q_OBJECT
private:
    enum sTypes
    {
        byte,
        word,
        dword
    };

public:
    explicit HexConverter(QWidget *parent = 0);
    ~HexConverter();

private:
    QLineEdit* lastEdited;

    const QIntValidator* sByte;
    const QIntValidator* sShort;
    const QIntValidator* sInt;

    const QIntValidator* uByte;
    const QIntValidator* uShort;
    const QIntValidator* uInt;

    const QRegExpValidator* hByte;
    const QRegExpValidator* hShort;
    const QRegExpValidator* hInt;

    sTypes sType = word;
    Ui::HexConverter *ui;

    void update();

private slots:
    void hexChanged(QString text);
    void decChanged(QString text);
    void on_rbyte_clicked();
    void on_rword_clicked();
    void on_rdword_clicked();
    void on_signSwitch_currentIndexChanged(int index);
};

#endif // HEXCONVERTER_H
