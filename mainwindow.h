#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMessageBox>
#include <QFileDialog>
#include <QTextStream>
#include <QFile>
#include <QList>
#include <QByteArray>
#include <QLineEdit>
#include <QTableView>
#include <QStandardItemModel>
#include <QTextCodec>
#include <QTextBrowser>
#include "debuggerui.h"
#include "compiler.h"
#include "hexconverter.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_actionExit_triggered();

    void on_actionOpen_triggered();

    void on_actionSave_triggered();

    void on_actionSave_As_triggered();

    void on_actionNew_triggered();

    void on_mainTextField_textChanged();

    void on_actionDebugger_triggered();

private:
    const int MemSize = 0x10FFF0;

    Ui::MainWindow *ui;

    QFileDialog *fileDialog;
    QTableView *asciiTable;
    HexConverter *hexConverter;
    QTextBrowser *helpViewer;

    QByteArray* memory;
    DebuggerUI* debug;

    Compiler* compiler;

    QString projectFileName = "";
    QString projectPath = "/";

    QList<int> opLengths;

    QHash<QString, Register> registers;

    bool asciiGenerated = false;
    bool unsavedChanges = false;

    bool openFile(QString fileName);
    bool writeFile(QString fileName);
    void asciiInit();

    void closeEvent(QCloseEvent *e);

private slots:
    bool updateCS();
    void on_actionASCII_Table_triggered();
    void on_actionHex_Converter_triggered();
    void on_actionHelp_Index_triggered();
    void updateTitle(bool reset);
signals:
    void fileNameChanged(bool reset = false);
};

#endif // MAINWINDOW_H
