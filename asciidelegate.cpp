#include "asciidelegate.h"

AsciiDelegate::AsciiDelegate(QWidget *parent) : QItemDelegate(parent) {}

QWidget* AsciiDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    LineDelegate *lineEdit = new LineDelegate(parent);

    connect(lineEdit, SIGNAL(changeRow(int)), this, SLOT(rowChanged(int)));
    connect(lineEdit, SIGNAL(editingFinished()), this, SLOT(commitAndCloseEditor()));
    return lineEdit;
}

void AsciiDelegate::rowChanged(int r)
{
    emit changeRow(r);
}

void AsciiDelegate::commitAndCloseEditor()
{
    LineDelegate *editor = qobject_cast<LineDelegate *>(sender());
    emit commitData(editor);
    emit closeEditor(editor);
}
