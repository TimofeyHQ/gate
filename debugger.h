#ifndef DEBUGGER_H
#define DEBUGGER_H

#include <unordered_map>
#include <fstream>
#include <regex>
#include <iostream>
#include <climits>
#include <QtDebug>
#include <QStringList>
#include <QTextStream>
#include <QThread>
#include <QObject>
#include <QByteArray>

#include "register.h"
#include "screen.h"

//ToDo next:
//Implement memory

class Debugger : public QObject
{
    Q_OBJECT

signals:
    void FieldsUpdated();
    void ScreenUpdated(bool forceShow = false);
    void MemoryUpdated(QByteArray ba, int pos);
    void error(int line);

private:
    enum aType // argument types:
    {
        undef, // could not define type, exception to be thrown
        num, // hex
        reg, // register
        mem, // (complex) memory expression
    };

    bool terminate = false;
    bool nextBP = false;
    int jumpTo = 0;

    QTextStream* kbStream = nullptr;
    QTextStream* errStream = nullptr;

    const int Base = 16;
    const int MaxArgs = 3;

    typedef QString* opArgs;
    typedef void (Debugger::*Op)(const opArgs args);

    const int linMemsize = 0x10FFF0; //max byte accessible is FFFF:FFFF

    QHash<QString, Register> registers;
    QHash<QString, Register>* defaulSegtRegPtr;

    QHash<QString, Op> instructions;
    QHash<QString, bool> flags;

    QByteArray memory;
    QByteArray* defaultMemoryPtr;

    Screen screen;
    bool splitAndExec(QString str);

    void mov(const opArgs args);
    void add(const opArgs args);
    void sub(const opArgs args);
    void mul(const opArgs args);
    void div(const opArgs args);
    void imul(const opArgs args);
    void idiv(const opArgs args);
    void push(const opArgs args);
    void pop(const opArgs args);
    void inc(const opArgs args);
    void dec(const opArgs args);
    void interrupt(const opArgs args);
    void cmp(const opArgs args);
    void jmp(const opArgs args);
    void je(const opArgs args);
    void jz(const opArgs args);
    void jne(const opArgs args);
    void jnz(const opArgs args);
    void jg(const opArgs args);
    void jl(const opArgs args);
    void jge(const opArgs args);
    void jle(const opArgs args);

    int getGlobalAddr(const unsigned short& seg, const unsigned short& off) const;

    aType getArgType(const QString &s) const;
    void resetFlags();

    short getMemVal(const QString &m, char width) const;
    void setMemVal(QString &m, short val, char width);

    void setRegVal(QString, unsigned short v);
public:
    void setStream(QTextStream* stream);

    void init(QByteArray * const _memory,
              QHash<QString, Register> * const segRegisters);
    void setErrStream(QTextStream *_errStream);
    void reset();
    bool parse(const QVector<QPair<int, QString> > &lines);
    bool debug(const QVector<QPair<int, QString> > &lines, const QVector<bool> &bPoints);

    bool getFlag(QString f) const;
    short getRegVal(QString r) const;

    const Screen& getScreen() const;

    Debugger(QObject *parent, QTextStream* kbStream = nullptr);
    ~Debugger();
public slots:
    void terminateASAP();
    void next();
};

#endif // DEBUGGER_H
