#ifndef ASCIIDELEGATE_H
#define ASCIIDELEGATE_H

#include <QItemDelegate>
#include "linedelegate.h"


class AsciiDelegate : public QItemDelegate
{
    Q_OBJECT
private:
    const int MaxChars = 16;
public:
    AsciiDelegate(QWidget *parent);
    QWidget* createEditor(QWidget *parent,
                          const QStyleOptionViewItem &option,
                          const QModelIndex &index) const;
signals:
    void changeRow(int r);
private slots:
    void rowChanged(int r);
    void commitAndCloseEditor();
};

#endif // ASCIIDELEGATE_H
