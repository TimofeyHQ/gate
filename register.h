//
// Created by chromean on 21/10/2015.
//

#ifndef ASMEMU_REGISTER_H
#define ASMEMU_REGISTER_H

#include <iostream>

class Register {
private:
    unsigned short val;
    unsigned char high = 0x0;
    unsigned char low = 0x0;

public:    
    void set(const unsigned short& s);
    void setLow(const unsigned char& b);
    void setHigh(const unsigned char& b);

    Register();
    Register(unsigned short int v);

    unsigned short get() const;
    unsigned char getLow() const;
    unsigned char getHigh() const;
};


#endif //ASMEMU_REGISTER_H
