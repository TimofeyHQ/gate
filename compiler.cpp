#include "compiler.h"

Compiler::aType Compiler::getArgType(const std::string &s) const
{
    std::regex num("^[[:xdigit:]]{1,4}$");
    std::regex reg("^(?:AX|CX|DX|BX|AH|AL|CH|CL|DH|DL|BH|BL|SP|BP|SI|DI)$");
    std::regex mem("^\\[(?:BX|SI|DI)]$");

    if (std::regex_match(s, num)) return aType::num;
    else if (std::regex_match(s, reg)) return aType::reg;
    else if (std::regex_match(s, mem)) return aType::mem;
    else return undef;
}

Compiler::Compiler() : errStream(new QTextStream(&buffer))
{
    opCodes = {
        {"MOV", std::vector<unsigned char> {0b10001000, 0b10110000}}, //first for reg or mem
        {"ADD", std::vector<unsigned char> {0b00000000, 0b10000000}}, //operations, second is
        {"SUB", std::vector<unsigned char> {0b00101000, 0b10000000}}, //for immediate value
        {"CMP", std::vector<unsigned char> {0b00111000, 0b10000000}},
        {"JMP", std::vector<unsigned char> {0b11111111}},
        {"MUL", std::vector<unsigned char> {0b11110110}},
        {"DIV", std::vector<unsigned char> {0b11110110}},
        {"IMUL", std::vector<unsigned char> {0b11110110}},
        {"IDIV", std::vector<unsigned char> {0b11110110}},
        {"INC", std::vector<unsigned char> {0b11111110}},
        {"DEC", std::vector<unsigned char> {0b11111110}}
    };
    jmpOps = {
        {"JE", 0x74},
        {"JZ", 0x74},
        {"JNE", 0x75},
        {"JNZ", 0x75},
        {"JG", 0x7f},
        {"JGE", 0x7d},
        {"JL", 0x7c},
        {"JLE", 0x7e},
    };
    registers = {
        {"AX", 0b000},
        {"CX", 0b001},
        {"DX", 0b010},
        {"BX", 0b011},
        {"SP", 0b100},
        {"BP", 0b101},
        {"SI", 0b110},
        {"DI", 0b111},
        {"AL", 0b000},
        {"CL", 0b001},
        {"DL", 0b010},
        {"BL", 0b011},
        {"AH", 0b100},
        {"CH", 0b101},
        {"DH", 0b110},
        {"BH", 0b111}
    };
    memOps =
    {
        {"[SI]", 0b100},
        {"[DI]", 0b101},
        {"[BX]", 0b111}
    };
}

Compiler::~Compiler()
{
    delete errStream;
}

QByteArray Compiler::add(std::string args[])
{
    QByteArray res;

    unsigned char opCode = 0;
    int argLength = 0;

    aType t1 = getArgType(args[1]);
    aType t2 = getArgType(args[2]);

    if(t1 == reg)
    {
        if(t2 == num)
        {
            unsigned short temp = std::stoi(args[2], 0, Base);

            opCode = opCodes[args[0]][1];
            argLength = 1;

            if(args[1].back() != 'H' && args[1].back() != 'L' && args[2].back() != 'H' && args[2].back() != 'L')
            {
                argLength = 2;
                opCode |= 0b00000001;
            }

            if(args[1] == "AX") //Intel shortcuts
            {
                res.append(0x05);
            }
            else if(args[1] == "AL")
            {
                res.append(0x04);
            }
            else
            {
                if(temp <= CHAR_MAX)
                {
                    opCode |= 0b00000010;
                    argLength = 1;
                }

                res.append(opCode);
                opCode = 0b11000000; //reg mode and REG field set to addition

                opCode |= registers[args[1]];

                res.append(opCode);
            }

            if (argLength == 2)
            {
                res.append(temp & 0x00ff);
                res.append((temp & 0xff00) >> 8);
            }
            else if (argLength == 1)
            {
                if(temp <= UCHAR_MAX) res.append(temp);
                else
                {
                    res.clear();
                    *errStream << "Number doesn't fit the target register";
                }
            }
        }
        else if(t2 == reg)
        {
            opCode = opCodes[args[0]][0];

            if(args[1].back() != 'H' && args[1].back() != 'L' && args[2].back() != 'H' && args[2].back() != 'L')
            {
                opCode |= 0b00000001; //set sidth to 16 bits
            }
            else if(!((args[1].back() == 'H' || args[1].back() == 'L') && (args[2].back() == 'H' || args[2].back() == 'L')))
            {
                *errStream << "Register sizes do not match";

                return res;
            }
            res.append(opCode);

            opCode = 0b11000000; //reg mode

            opCode |= (registers[args[2]] << 3);
            opCode |= registers[args[1]];

            res.append(opCode);
        }
        else if(t2 == mem)
        {
            opCode = opCodes[args[0]][0];

            opCode |= 0b00000010; //direction bit set to Memory >> Reg

            if(args[1].back() != 'H' && args[1].back() != 'L' && args[2].back() != 'H' && args[2].back() != 'L')
            {
                opCode |= 0b00000001;
            }
            res.append(opCode);

            opCode = 0;

            opCode |= (registers[args[1]] << 3);
            opCode |= memOps[args[2]];

            res.append(opCode);
        }
        else *errStream << "Unsupported second argument type";
    }
    else if(t1 == mem)
    {
        if(t2 == reg)
        {
            opCode = opCodes[args[0]][0]; //direction bit set to Memory << Reg

            if(args[1].back() != 'H' && args[1].back() != 'L' && args[2].back() != 'H' && args[2].back() != 'L')
            {
                opCode |= 0b00000001;
            }
            res.append(opCode);

            opCode = 0;

            opCode |= (registers[args[2]] << 3);
            opCode |= memOps[args[1]];

            res.append(opCode);
        }
        else *errStream << "Unsupported second argument type";
    }
    else *errStream << "Unsupported first argument type";

    return res;
}

QByteArray Compiler::sub(std::string args[])
{
    QByteArray res;

    unsigned char opCode = 0;
    int argLength = 0;

    aType t1 = getArgType(args[1]);
    aType t2 = getArgType(args[2]);

    if(t1 == reg)
    {
        if(t2 == num)
        {
            unsigned short temp = std::stoi(args[2], 0, Base);

            opCode = opCodes[args[0]][1];
            argLength = 1;

            if(args[1].back() != 'H' && args[1].back() != 'L' && args[2].back() != 'H' && args[2].back() != 'L')
            {
                argLength = 2;
                opCode |= 0b00000001;
            }

            if(args[1] == "AX") //Intel shortcuts
            {
                res.append(0x2D);
            }
            else if(args[1] == "AL")
            {
                res.append(0x2C);
            }
            else
            {
                if(temp <= CHAR_MAX)
                {
                    opCode |= 0b00000010;
                    argLength = 1;
                }

                res.append(opCode);
                opCode = 0b11101000; //reg mode and REG field set to subtraction

                opCode |= registers[args[1]];

                res.append(opCode);
            }

            if (argLength == 2)
            {
                res.append(temp & 0x00ff);
                res.append((temp & 0xff00) >> 8);
            }
            else if (argLength == 1)
            {
                if(temp <= UCHAR_MAX) res.append(temp);
                else
                {
                    res.clear();
                    *errStream << "Number doesn't fit the target register";
                }
            }
        }
        else if(t2 == reg)
        {
            opCode = opCodes[args[0]][0];

            if(args[1].back() != 'H' && args[1].back() != 'L' && args[2].back() != 'H' && args[2].back() != 'L')
            {
                opCode |= 0b00000001; //set sidth to 16 bits
            }
            else if(!((args[1].back() == 'H' || args[1].back() == 'L') && (args[2].back() == 'H' || args[2].back() == 'L')))
            {
                *errStream << "Register sizes do not match";

                return res;
            }
            res.append(opCode);

            opCode = 0b11000000; //reg mode

            opCode |= (registers[args[2]] << 3);
            opCode |= registers[args[1]];

            res.append(opCode);
        }else if(t2 == mem)
        {
            opCode = opCodes[args[0]][0];

            opCode |= 0b00000010; //direction bit set to Memory >> Reg

            if(args[1].back() != 'H' && args[1].back() != 'L' && args[2].back() != 'H' && args[2].back() != 'L')
            {
                opCode |= 0b00000001;
            }
            res.append(opCode);

            opCode = 0;

            opCode |= (registers[args[1]] << 3);
            opCode |= memOps[args[2]];

            res.append(opCode);
        }
        else *errStream << "Unsupported second argument type";
    }
    else if(t1 == mem)
    {
        if(t2 == reg)
        {
            opCode = opCodes[args[0]][0]; //direction bit set to Memory << Reg

            if(args[1].back() != 'H' && args[1].back() != 'L' && args[2].back() != 'H' && args[2].back() != 'L')
            {
                opCode |= 0b00000001;
            }
            res.append(opCode);

            opCode = 0;

            opCode |= (registers[args[2]] << 3);
            opCode |= memOps[args[1]];

            res.append(opCode);
        }
        else *errStream << "Unsupported second argument type";
    }
    else *errStream << "Unsupported first argument type";

    return res;
}

QByteArray Compiler::mov(std::string args[])
{
    QByteArray res;

    unsigned char opCode = 0;
    int argLength = 0;

    aType t1 = getArgType(args[1]);
    aType t2 = getArgType(args[2]);

    if(t1 == reg)
    {
        if(t2 == num)
        {
            opCode = opCodes[args[0]][1];
            argLength = 1;

            if(args[1].back() != 'H' && args[1].back() != 'L' && args[2].back() != 'H' && args[2].back() != 'L')
            {
                argLength = 2;
                opCode |= 0b00001000;
            }

            opCode |= registers[args[1]];

            res.append(opCode);

            if (argLength == 2)
            {
                unsigned short temp = std::stoi(args[2], 0, Base);
                res.append(temp & 0x00ff);
                res.append((temp & 0xff00) >> 8);
            }
            else if (argLength == 1)
            {
                unsigned short temp = std::stoi(args[2], 0, Base);
                if(temp <= UCHAR_MAX) res.append(temp);
                else
                {
                    res.clear();
                    *errStream << "Number doesn't fit the target register";
                }
            }
        }
        else if(t2 == reg)
        {
            opCode = opCodes[args[0]][0];

            if(args[1].back() != 'H' && args[1].back() != 'L' && args[2].back() != 'H' && args[2].back() != 'L')
            {
                opCode |= 0b00000001; //set sidth to 16 bits
            }
            else if(!((args[1].back() == 'H' || args[1].back() == 'L') && (args[2].back() == 'H' || args[2].back() == 'L')))
            {
                *errStream << "Register sizes do not match";

                return res;
            }
            res.append(opCode);

            opCode = 0b11000000; //Register mode

            opCode |= (registers[args[2]] << 3);
            opCode |= registers[args[1]];

            res.append(opCode);
        }
        else if(t2 == mem)
        {
            opCode = opCodes[args[0]][0];

            opCode |= 0b00000010; //direction bit set to Memory >> Reg

            if(args[1].back() != 'H' && args[1].back() != 'L' && args[2].back() != 'H' && args[2].back() != 'L')
            {
                opCode |= 0b00000001;
            }
            res.append(opCode);

            opCode = 0;

            opCode |= (registers[args[1]] << 3);
            opCode |= memOps[args[2]];

            res.append(opCode);
        }
        else *errStream << "Unsupported second argument type";
    }
    else if(t1 == mem)
    {
        if(t2 == reg)
        {
            opCode = opCodes[args[0]][0]; //direction bit set to Memory << Reg

            if(args[1].back() != 'H' && args[1].back() != 'L' && args[2].back() != 'H' && args[2].back() != 'L')
            {
                opCode |= 0b00000001;
            }
            res.append(opCode);

            opCode = 0;

            opCode |= (registers[args[2]] << 3);
            opCode |= memOps[args[1]];

            res.append(opCode);
        }
        else *errStream << "Unsupported second argument type";
    }
    else *errStream << "Unsupported first argument type";

    return res;
}

QByteArray Compiler::inc(std::string args[])
{
    QByteArray res;

    unsigned char opCode = 0;

    aType t1 = getArgType(args[1]);

    if(t1 == reg)
    {
        opCode = opCodes[args[0]][0];

        if(args[1] == "AX") //Intel shortcuts
        {
            res.append(0x40);
        }
        else if(args[1] == "BX")
        {
            res.append(0x43);
        }
        else if(args[1] == "CX")
        {
            res.append(0x41);
        }
        else if(args[1] == "DX")
        {
            res.append(0x42);
        }
        else
        {
            res.append(opCode);

            opCode = 0b11000000; //reg mode and REG field set to increment

            opCode |= registers[args[1]];

            res.append(opCode);
        }
    }
    return res;
}

QByteArray Compiler::dec(std::string args[])
{
    QByteArray res;

    unsigned char opCode = 0;

    aType t1 = getArgType(args[1]);

    if(t1 == reg)
    {
        opCode = opCodes[args[0]][0];

        if(args[1] == "AX") //Intel shortcuts
        {
            res.append(0x48);
        }
        else if(args[1] == "BX")
        {
            res.append(0x4B);
        }
        else if(args[1] == "CX")
        {
            res.append(0x49);
        }
        else if(args[1] == "DX")
        {
            res.append(0x4A);
        }
        else
        {
            res.append(opCode);

            opCode = 0b11001000; //reg mode and REG field set to decrement

            opCode |= registers[args[1]];

            res.append(opCode);
        }
    }
    return res;
}

QByteArray Compiler::mul(std::string args[])
{
    QByteArray res;

    unsigned char opCode = 0;

    aType t1 = getArgType(args[1]);

    if(t1 == reg)
    {
        opCode = opCodes[args[0]][0];

        if(args[1].back() != 'H' && args[1].back() != 'L' && args[2].back() != 'H' && args[2].back() != 'L')
        {
            opCode |= 0b00000001;
        }

        res.append(opCode);
        opCode = 0b11100000; //reg mode and REG field set to multiplication

        opCode |= registers[args[1]];
        res.append(opCode);
    }
    return res;
}

QByteArray Compiler::div(std::string args[])
{
    QByteArray res;

    unsigned char opCode = 0;

    aType t1 = getArgType(args[1]);

    if(t1 == reg)
    {
        opCode = opCodes[args[0]][0];

        if(args[1].back() != 'H' && args[1].back() != 'L' && args[2].back() != 'H' && args[2].back() != 'L')
        {
            opCode |= 0b00000001;
        }

        res.append(opCode);
        opCode = 0b11110000; //reg mode and REG field set to division

        opCode |= registers[args[1]];
        res.append(opCode);
    }
    return res;
}

QByteArray Compiler::imul(std::string args[])
{
    QByteArray res;

    unsigned char opCode = 0;

    aType t1 = getArgType(args[1]);

    if(t1 == reg)
    {
        opCode = opCodes[args[0]][0];

        if(args[1].back() != 'H' && args[1].back() != 'L' && args[2].back() != 'H' && args[2].back() != 'L')
        {
            opCode |= 0b00000001;
        }

        res.append(opCode);
        opCode = 0b11101000; //reg mode and REG field set to signed multiplication

        opCode |= registers[args[1]];
        res.append(opCode);
    }
    return res;
}

QByteArray Compiler::idiv(std::string args[])
{
    QByteArray res;

    unsigned char opCode = 0;

    aType t1 = getArgType(args[1]);

    if(t1 == reg)
    {
        opCode = opCodes[args[0]][0];

        if(args[1].back() != 'H' && args[1].back() != 'L' && args[2].back() != 'H' && args[2].back() != 'L')
        {
            opCode |= 0b00000001;
        }

        res.append(opCode);
        opCode = 0b11111000;  //reg mode and REG field set to signed division

        opCode |= registers[args[1]];
        res.append(opCode);
    }
    return res;
}

QByteArray Compiler::push(std::string args[])
{
    QByteArray res;

    aType t1 = getArgType(args[1]);

    if(t1 == reg)
    {
        if(args[1] == "AX") //Intel shortcuts
        {
            res.append(0x50);
        }
        else if(args[1] == "BX")
        {
            res.append(0x53);
        }
        else if(args[1] == "CX")
        {
            res.append(0x51);
        }
        else if(args[1] == "DX")
        {
            res.append(0x52);
        }
        else if(args[1] == "SI")
        {
            res.append(0x56);
        }
        else if(args[1] == "SP")
        {
            res.append(0x54);
        }
        else if(args[1] == "DI")
        {
            res.append(0x57);
        }
        else if(args[1] == "BP")
        {
            res.append(0x55);
        }
        else *errStream << "Unsupported registry";
    }    
    else *errStream << "Unsupported argument type";

    return res;
}

QByteArray Compiler::pop(std::string args[])
{
    QByteArray res;

    aType t1 = getArgType(args[1]);

    if(t1 == reg)
    {
        if(args[1] == "AX") //Intel shortcuts
        {
            res.append(0x58);
        }
        else if(args[1] == "BX")
        {
            res.append(0x5B);
        }
        else if(args[1] == "CX")
        {
            res.append(0x59);
        }
        else if(args[1] == "DX")
        {
            res.append(0x5A);
        }
        else if(args[1] == "SI")
        {
            res.append(0x5e);
        }
        else if(args[1] == "SP")
        {
            res.append(0x5c);
        }
        else if(args[1] == "DI")
        {
            res.append(0x5f);
        }
        else if(args[1] == "BP")
        {
            res.append(0x5d);
        }
        else *errStream << "Unsupported registry";
    }
    else *errStream << "Unsupported argument type";

    return res;
}

QByteArray Compiler::interrupt(std::string args[])
{
    QByteArray res;

    aType t1 = getArgType(args[1]);

    if(t1 == num)
    {
        int code = std::stoi(args[1], 0, Base);

        if(code == 0x21 || code == 0x10 || code == 0x20)
        {
            res.append(0xCD);
            res.append(code);
        }
        else *errStream << "Unsupported Interrupt";
    }
    else *errStream << "Unsupported Argument";

    return res;
}

QByteArray Compiler::cmp(std::string args[])
{
    QByteArray res;

    unsigned char opCode = 0;
    int argLength = 0;

    aType t1 = getArgType(args[1]);
    aType t2 = getArgType(args[2]);

    if(t1 == reg)
    {
        if(t2 == num)
        {
            unsigned short temp = std::stoi(args[2], 0, Base);

            opCode = opCodes[args[0]][1];
            argLength = 1;

            if(args[1].back() != 'H' && args[1].back() != 'L' && args[2].back() != 'H' && args[2].back() != 'L')
            {
                argLength = 2;
                opCode |= 0b00000001;
            }

            if(args[1] == "AX") //Intel shortcuts
            {
                res.append(0x3D);
            }
            else if(args[1] == "AL") //Intel shortcuts
            {
                res.append(0x3C);
            }
            else
            {
                if(temp <= CHAR_MAX)
                {
                    opCode |= 0b00000010;
                    argLength = 1;
                }

                res.append(opCode);
                opCode = 0b11111000; //reg mode and REG field set to compare immediate value

                opCode |= registers[args[1]];

                res.append(opCode);
            }

            if (argLength == 2)
            {
                res.append(temp & 0x00ff);
                res.append((temp & 0xff00) >> 8);
            }
            else if (argLength == 1)
            {
                if(temp <= UCHAR_MAX) res.append(temp);
                else
                {
                    res.clear();
                    *errStream << "Number doesn't fit the target register";
                }
            }
        }
        else if(t2 == reg)
        {
            opCode = opCodes[args[0]][0];

            if(args[1].back() != 'H' && args[1].back() != 'L' && args[2].back() != 'H' && args[2].back() != 'L')
            {
                opCode |= 0b00000001; //set sidth to 16 bits
            }
            else if(!((args[1].back() == 'H' || args[1].back() == 'L') && (args[2].back() == 'H' || args[2].back() == 'L')))
            {
                *errStream << "Register sizes do not match";

                return res;
            }
            res.append(opCode);

            opCode = 0b11000000; //reg mode

            opCode |= (registers[args[2]] << 3);
            opCode |= registers[args[1]];

            res.append(opCode);
        }
        else if(t2 == mem)
        {
            opCode = opCodes[args[0]][0];

            opCode |= 0b00000010; //direction bit set to Memory >> Reg

            if(args[1].back() != 'H' && args[1].back() != 'L' && args[2].back() != 'H' && args[2].back() != 'L')
            {
                opCode |= 0b00000001;
            }
            res.append(opCode);

            opCode = 0;

            opCode |= (registers[args[1]] << 3);
            opCode |= memOps[args[2]];

            res.append(opCode);
        }
        else *errStream << "Unsupported second argument type";
    }
    else if(t1 == mem)
    {
        if(t2 == reg)
        {
            opCode = opCodes[args[0]][0]; //direction bit set to Memory << Reg

            if(args[1].back() != 'H' && args[1].back() != 'L' && args[2].back() != 'H' && args[2].back() != 'L')
            {
                opCode |= 0b00000001;
            }
            res.append(opCode);

            opCode = 0;

            opCode |= (registers[args[2]] << 3);
            opCode |= memOps[args[1]];

            res.append(opCode);
        }
        else *errStream << "Unsupported second argument type";
    }
    else *errStream << "Unsupported first argument type";

    return res;
}

QByteArray Compiler::jmp(std::string args[])
{
    QByteArray res;

    unsigned char opCode = 0;

    aType t1 = getArgType(args[1]);

    if(t1 == num)
    {
        unsigned short n = std::stoi(args[1], 0, Base);

        int diff = n - currentAddr;

        if(diff >= (CHAR_MIN + 2) && diff <= (CHAR_MAX + 2))
        {
            res.append(0xEB);

            res.append(diff - 2);
        }
        else if (diff >= (SHRT_MIN + 3) && diff <= (SHRT_MAX + 3))
        {
            res.append(0xE9);

            diff -= 3;

            res.append(diff & 0x00ff);
            res.append((diff >> 8) & 0x00ff);
        }
        else *errStream << "Unsupported first argument type";
    }
    else if(t1 == reg)
    {
        if(args[1].back() != 'H' && args[1].back() != 'L')
        {
            res.append(opCodes[args[0]][0]);

            opCode = 0b11100000; //Mod and Reg set to Register Op;

            opCode |= registers[args[1]];

            res.append(opCode);
        }
        else *errStream << "Unsupported first argument type";
    }
    else if(t1 == mem)
    {
        res.append(opCodes[args[0]][0]);

        opCode = 0b00100000; //Mod and Reg set to Memory Op;
        opCode |= memOps[args[1]];

        res.append(opCode);
    }
    else *errStream << "Unsupported first argument type";

    return res;
}

QByteArray Compiler::condJmp(std::string args[])
{
    QByteArray res;

    aType t1 = getArgType(args[1]);

    if(t1 == num)
    {
        unsigned short n = std::stoi(args[1], 0, Base);

        int diff = n - currentAddr - 2;

        if(diff >= CHAR_MIN && diff <= CHAR_MAX)
        {
            res.append(jmpOps[args[0]]);

            res.append(diff);
        }
        else *errStream << "Unsupported first argument type";
    }
    else *errStream << "Unsupported first argument type";

    return res;
}


QByteArray Compiler::compile(const QStringList &src, QList<int>& opLengths, QStringList& errList)
{
    currentAddr = 0x100;
    int line = 1;
    opLengths.clear();

    QByteArray codeSegment;

    unsigned char memCur = 0;

    std::regex match("([[:upper:]]{2,}) +([[:upper:]]{2}|[[:xdigit:]]+|\\[[[:upper:]]{2}])(?:, *([[:upper:]]{2}|[[:xdigit:]]+|\\[[[:upper:]]{2}])|) *$");
    std::regex empty("^[ \t]*$");

    for (QString i : src)
    {
        std::string str;

        QByteArray res;

        str = i.mid(0, i.indexOf(';')).toStdString(); //chop comments off

        if(!std::regex_match(str, empty))
        {
            std::transform(str.begin(), str.end(), str.begin(), ::toupper);

            std::string args[MaxArgs];

            std::sregex_iterator next(str.begin(), str.end(), match);
            std::sregex_iterator end;

            while (next != end) {
                std::smatch res = *next;
                args[0] = res[1];
                args[1] = res[2];
                args[2] = res[3];
                ++next;
            }

            errStream->readAll();

            if (args[0] == "MOV") res = mov(args);
            else if (args[0] == "ADD") res = add(args);
            else if (args[0] == "SUB") res = sub(args);
            else if (args[0] == "INC") res = inc(args);
            else if (args[0] == "DEC") res = dec(args);
            else if (args[0] == "MUL") res = mul(args);
            else if (args[0] == "DIV") res = div(args);
            else if (args[0] == "IMUL") res = imul(args);
            else if (args[0] == "IDIV") res = idiv(args);
            else if (args[0] == "PUSH") res = push(args);
            else if (args[0] == "POP") res = pop(args);
            else if (args[0] == "INT") res = interrupt(args);
            else if (args[0] == "CMP") res = cmp(args);
            else if (args[0] == "JMP") res = jmp(args);
            else if (args[0] == "JE" || args[0] == "JZ") res = condJmp(args);
            else if (args[0] == "JNE" || args[0] == "JNZ") res = condJmp(args);
            else if (args[0] == "JG" || args[0] == "JGE") res = condJmp(args);
            else if (args[0] == "JL" || args[0] == "JLE") res = condJmp(args);
            else *errStream << "Could not parse the instruction";

            if(!errStream->atEnd())
            {
                QString temp;

                temp = errStream->readLine();
                temp.prepend("Line: " + QString::number(line) + " (CS:" + QString::number(currentAddr, Base).rightJustified(4, '0') + ") ");

                errList.append(temp);
            }
        }

        codeSegment.insert(memCur, res);
        currentAddr += res.size();
        opLengths.append(res.size());

        memCur += res.size();
        ++line;
    }

    return codeSegment;
}

QByteArray Compiler::compileSingleString(const QString &_src)
{
    QByteArray res;

    std::regex match("([[:upper:]]{3,}) +([[:upper:]]{2}|[[:xdigit:]]+|\[[:[:upper:][:xdigit:]]{2,}])(?:, *([[:upper:]]{2}|[[:xdigit:]]+|\[[:[:upper:][:xdigit:]]{2,}]|))");
    std::string str;

    str = _src.toStdString();

    std::transform(str.begin(), str.end(), str.begin(), ::toupper);

    std::string args[MaxArgs];

    std::sregex_iterator next(str.begin(), str.end(), match);
    std::sregex_iterator end;

    while (next != end) {
        std::smatch res = *next;
        args[0] = res[1];
        args[1] = res[2];
        args[2] = res[3];
        ++next;
    }

    if(args[0] == "MOV") res = mov(args);
    else if(args[0] == "ADD") res = add(args);
    else if(args[0] == "SUB") res = sub(args);

    return res;
}
