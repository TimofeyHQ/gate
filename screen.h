#ifndef SCREEN_H
#define SCREEN_H

#include <vector>
#include <iostream>
#include <QImage>
#include <QTextCodec>
#include <unordered_map>
#include <QDebug>

class Screen
{
public:
    enum ScreenType {
        text = 0,
        graphics16 = 1,
        graphics64 = 2
    };

private:
    static const int ScreenTypeN = 4; //some unexplainable crash occured once

    ScreenType sType;

    std::pair<int, int> screenSize[ScreenTypeN];

    std::vector<std::vector<QChar>> charField;

    QTextCodec* cyrillic;

    QImage image;

    std::pair<int, int> cursor;

    std::unordered_map<int, QRgb> colors;

public:
    void cls(unsigned short begin, unsigned short end, unsigned char color);
    void reset();

    Screen();
    ~Screen();

    const std::pair<int, int>& getCursorPos() const;
    void setCursorPos(const std::pair<int, int> pos);

    void print(const char *c);
    void drawPoint(const unsigned int &v, const unsigned int &h, const char& color);
    void setPalette(const char& color);

    void newLine();

    const ScreenType& getScreenType() const;
    const int& getScreenHeight() const;
    const int& getScreenWidth() const;

    const std::vector<std::vector<QChar> > &getCharField() const;

    QImage getImage() const;

    void setScreenType(ScreenType t);
};

#endif // SCREEN_H
